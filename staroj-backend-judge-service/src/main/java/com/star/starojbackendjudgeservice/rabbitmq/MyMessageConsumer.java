package com.star.starojbackendjudgeservice.rabbitmq;

import com.rabbitmq.client.Channel;

import com.star.starojbackendjudgeservice.judge.JudgeService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
@Slf4j
public class MyMessageConsumer {

    @Resource
    private JudgeService judgeService;

    // 指定程序监听的消息队列和确认机制
    @SneakyThrows
    @RabbitListener(queues = {"code_queue"}, ackMode = "MANUAL")
    public void receiveMessage(String message, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag) {
        log.info("receiveMessage message = {}", message);
        long questionSubmitId = Long.parseLong(message);
        try {
            judgeService.doJudge(questionSubmitId);
            channel.basicAck(deliveryTag, false);
        } catch (Exception e) {
            /**
             * 1. deliveryTag：表示消息的唯一标识。每个消息都有一个唯一的deliveryTag，用于标识消息在channel中的顺序。
             * 当消费者接收到消息后，需要调用channel.basicAck方法并传递deliveryTag来确认消息的处理。
             *
             * 2. multiple：表示是否批量确认消息。当multiple为false时，只确认当前deliveryTag对应的消息；
             * 当multiple为true时，会确认当前deliveryTag及之前所有未确认的消息。
             */
            channel.basicNack(deliveryTag, false, false);
        }
    }

}
