package com.star.starojbackendjudgeservice.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * 用于创建测试程序用到的交换机和队列（只用在程序启动前执行一次）
 */
@Slf4j
@Component
public class InitRabbitMqBean {

    @Value("${spring.rabbitmq.host:localhost}")
    private String host;


    @PostConstruct
    public void init() {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost(host);
            factory.setUsername("guest");
            factory.setPassword("guest");
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();
            String EXCHANGE_NAME = "code_exchange";
            channel.exchangeDeclare(EXCHANGE_NAME, "direct");

//            // 创建死信交换机
//            String DEAD_EXCHANGE_NAME = "dead-direct-exchange";
//            channel.exchangeDeclare(DEAD_EXCHANGE_NAME, "direct");
//            // 创建死信队列，随机分配一个队列名称
//            String deadQueueName = "dead_queue";
//            channel.queueDeclare(deadQueueName, true, false, false, null);
//            channel.queueBind(deadQueueName, DEAD_EXCHANGE_NAME,  "dead_routingKey");
//            //绑定死信交换机(绑定消费者队列)


            // 创建队列，随机分配一个队列名称
            String queueName = "code_queue";
//            Map<String, Object> argv = new HashMap<>();
//            // 要绑定到哪个交换机
//            argv.put("x-dead-letter-exchange", "dead-direct-exchange");
//            // 指定死信要转发到哪个死信队列
//            argv.put("x-dead-letter-routing-key", "dead_routingKey");
            channel.queueDeclare(queueName, true, false, false, null);
            channel.queueBind(queueName, EXCHANGE_NAME, "my_routingKey");
            log.info("消息队列启动成功");
        } catch (Exception e) {
            log.error("消息队列启动失败", e);
        }
    }
}
