package com.star.starojbackendjudgeservice.judge.strategy;

import com.star.starojbackendmodel.model.model.codesandbox.JudgeInfo;


public interface JudgeStrategy {

    /**
     * 执行判题
     * @param judgeContext
     * @return
     */
    JudgeInfo doJudge(JudgeContext judgeContext);

}
