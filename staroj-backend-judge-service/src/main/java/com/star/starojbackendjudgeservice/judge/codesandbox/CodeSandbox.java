package com.star.starojbackendjudgeservice.judge.codesandbox;

import com.star.starojbackendmodel.model.model.codesandbox.ExecuteCodeRequest;
import com.star.starojbackendmodel.model.model.codesandbox.ExecuteCodeResponse;


/**
 * 代码沙箱接口定义
 */
public interface CodeSandbox {

    /**
     * 执行代码
     */
    ExecuteCodeResponse executeCode(ExecuteCodeRequest executeCodeRequest);
}
