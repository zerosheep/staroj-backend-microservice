package com.star.starojbackendjudgeservice.judge.codesandbox.impl;

import com.star.starojbackendjudgeservice.judge.codesandbox.CodeSandbox;
import com.star.starojbackendmodel.model.model.codesandbox.ExecuteCodeRequest;
import com.star.starojbackendmodel.model.model.codesandbox.ExecuteCodeResponse;
import com.star.starojbackendmodel.model.model.codesandbox.JudgeInfo;
import com.star.starojbackendmodel.model.model.enums.JudgeInfoMessageEnum;
import com.star.starojbackendmodel.model.model.enums.QuestionSubmitStatusEnum;


import java.util.List;

public class ExampleCodeSandbox implements CodeSandbox {
    @Override
    public ExecuteCodeResponse executeCode(ExecuteCodeRequest executeCodeRequest) {
        List<String> inputList = executeCodeRequest.getInputList();
        ExecuteCodeResponse executeCodeResponse = new ExecuteCodeResponse();
        executeCodeResponse.setOutputList(inputList);
        executeCodeResponse.setMessage("执行测试用例");
        //代码执行的状态
        executeCodeResponse.setStatus(QuestionSubmitStatusEnum.SUCCEED.getValue());
        JudgeInfo judgeInfo = new JudgeInfo();
        //题目 判题信息枚举
        judgeInfo.setMessage(JudgeInfoMessageEnum.ACCEPTED.getText());
        judgeInfo.setMemory(100L);
        judgeInfo.setTime(100L);
        executeCodeResponse.setJudgeInfo(judgeInfo);


        return executeCodeResponse;
    }
}
