package com.star.starojbackendjudgeservice.judge;


import com.star.starojbackendjudgeservice.judge.strategy.DefaultJudgeStrategy;
import com.star.starojbackendjudgeservice.judge.strategy.JudgeContext;
import com.star.starojbackendjudgeservice.judge.strategy.JudgeStrategy;
import com.star.starojbackendmodel.model.model.codesandbox.JudgeInfo;
import com.star.starojbackendmodel.model.model.entity.QuestionSubmit;
import org.springframework.stereotype.Service;

@Service
public class JudgeManager {
    /**
     * 判题服务
     */

    JudgeInfo doJudge(JudgeContext judgeContext) {
        QuestionSubmit questionSubmit = judgeContext.getQuestionSubmit();
        String language = questionSubmit.getLanguage();
        JudgeStrategy judgeStrategy = new DefaultJudgeStrategy();
        return judgeStrategy.doJudge(judgeContext);
    }


}
