package com.star.starojbackendjudgeservice.judge.codesandbox;

import com.star.starojbackendjudgeservice.judge.codesandbox.impl.ExampleCodeSandbox;
import com.star.starojbackendjudgeservice.judge.codesandbox.impl.RemoteCodeSandbox;
import com.star.starojbackendjudgeservice.judge.codesandbox.impl.ThirdPartyCodeSandbox;


/**
 * 代码沙箱工厂
 */
public class CodeSandBoxFactory {
    /**
     * 创建代码沙箱实例
     */

    public static CodeSandbox newInstance(String type) {
        switch (type) {
            case "example":
                return new ExampleCodeSandbox();
            case "remote":
                return new RemoteCodeSandbox();
            case "thirdParty":
                return new ThirdPartyCodeSandbox();
            default:
                return new ExampleCodeSandbox();
        }
    }
}
