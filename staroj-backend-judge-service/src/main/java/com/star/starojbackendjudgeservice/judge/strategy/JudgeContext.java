package com.star.starojbackendjudgeservice.judge.strategy;

import com.star.starojbackendmodel.model.model.codesandbox.JudgeInfo;
import com.star.starojbackendmodel.model.model.dto.question.JudgeCase;
import com.star.starojbackendmodel.model.model.entity.Question;
import com.star.starojbackendmodel.model.model.entity.QuestionSubmit;

import lombok.Data;

import java.util.List;

@Data
public class JudgeContext {

    private JudgeInfo judgeInfo;

    private List<String> inputList;

    private List<String> outputList;

    private List<JudgeCase> judgeCaseList;

    private Question question;

    private QuestionSubmit questionSubmit;

}
