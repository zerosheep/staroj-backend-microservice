package com.star.starojbackenduserservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@MapperScan("com.star.starojbackenduserservice.mapper")
@EnableScheduling
@EnableAspectJAutoProxy(proxyTargetClass = true, exposeProxy = true)
@ComponentScan("com.star")
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.star.starojbackendserviceclient.service"})
public class StarojBackendUserServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(StarojBackendUserServiceApplication.class, args);
    }

}
