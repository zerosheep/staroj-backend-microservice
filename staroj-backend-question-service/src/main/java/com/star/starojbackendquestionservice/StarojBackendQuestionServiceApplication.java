package com.star.starojbackendquestionservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@MapperScan("com.star.starojbackendquestionservice.mapper")
@EnableScheduling
@EnableAspectJAutoProxy(proxyTargetClass = true, exposeProxy = true)
@ComponentScan("com.star")
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.star.starojbackendserviceclient.service"})
public class StarojBackendQuestionServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(StarojBackendQuestionServiceApplication.class, args);
    }

}
