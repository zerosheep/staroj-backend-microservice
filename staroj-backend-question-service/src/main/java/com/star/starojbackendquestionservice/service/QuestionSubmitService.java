package com.star.starojbackendquestionservice.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.star.starojbackendmodel.model.model.dto.questionsubmit.QuestionSubmitAddRequest;
import com.star.starojbackendmodel.model.model.dto.questionsubmit.QuestionSubmitQueryRequest;
import com.star.starojbackendmodel.model.model.entity.QuestionSubmit;
import com.star.starojbackendmodel.model.model.entity.User;
import com.star.starojbackendmodel.model.model.vo.QuestionSubmitVO;


/**
* @author hp
* @description 针对表【question_submit(题目提交)】的数据库操作Service
* @createDate 2023-09-16 22:34:01
*/
public interface QuestionSubmitService extends IService<QuestionSubmit> {


    long doQuestionSubmit(QuestionSubmitAddRequest questionSubmitAddRequest, User loginUser);

    /**
     * 获取查询条件
     *
     * @param questionSubmitQueryRequest
     * @return
     */

    QueryWrapper<QuestionSubmit> getQueryWrapper(QuestionSubmitQueryRequest questionSubmitQueryRequest);

    /**
     * 获取题目封装
     *
     * @param questionSubmit
     * @param loginUser
     * @return
     */
    QuestionSubmitVO getQuestionSubmitVO(QuestionSubmit questionSubmit, User loginUser);

    /**
     * 分页获取题目封装
     *
     * @param questionSubmitPage
     * @param loginUser
     * @return
     */
    Page<QuestionSubmitVO> getQuestionSubmitVOPage(Page<QuestionSubmit> questionSubmitPage, User loginUser);

}
