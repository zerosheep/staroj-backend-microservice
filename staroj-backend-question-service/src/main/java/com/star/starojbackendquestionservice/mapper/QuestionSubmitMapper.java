package com.star.starojbackendquestionservice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.star.starojbackendmodel.model.model.entity.QuestionSubmit;


/**
* @author hp
* @description 针对表【question_submit(题目提交)】的数据库操作Mapper
* @createDate 2023-09-16 22:34:01
* @Entity com.yupi.staroj.model.entity.QuestionSubmit
*/
public interface QuestionSubmitMapper extends BaseMapper<QuestionSubmit> {

}




