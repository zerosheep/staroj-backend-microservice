package com.star.starojbackendquestionservice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.star.starojbackendmodel.model.model.entity.Question;


/**
* @author hp
* @description 针对表【question(题目)】的数据库操作Mapper
* @createDate 2023-09-16 22:33:45
* @Entity com.yupi.staroj.model.entity.Question
*/
public interface QuestionMapper extends BaseMapper<Question> {

}




