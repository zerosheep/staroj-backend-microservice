package com.star.starojbackendgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class StarojBackendGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(StarojBackendGatewayApplication.class, args);
    }

}
